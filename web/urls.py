# myobject/web/urls.py
from django.conf.urls import url

from web.views import index,cart,list
urlpatterns = [
   url(r'^$', index.index, name="web_index"),#前台首页
   url(r'^login$',index.login, name="web_login"),#登陆
   url(r'^dologin$',index.dologin, name="web_dologin"),#执行登陆
   url(r'^logout$',index.logout, name="web_logout"),#执行退出


   #购物车管理路由配置
   url(r'^cart$', cart.add, name="cart_add"),


   #订单路由
   url(r'^list$',list.index,name="web_list"),
]