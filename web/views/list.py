from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect
from django.core.urlresolvers import reverse
from common.models import Payment,Orders,Product

def loadinfo(request):
	lists = Payment.objects.filter(pid=0)
	context = {'paylist':lists}
	return context


def index(request):
    '''浏览订单信息'''
    context = loadinfo(request)
    #获取当前登录者的订单信息
    odlist = Orders.objects.filter(uid=request.session['vipuser']['id'])
    #遍历订单信息，查询对应的详情信息
    for od in odlist:
        prlist = Product.objects.filter(orderid=od.id)
        od.prolist = prlist
    context['orderslist'] = odlist
    return render(request,"web/list.html",context)


