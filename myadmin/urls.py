# myobject/myadmin/urls.py
from django.conf.urls import url

from myadmin.view import users,shops,index,type,goods

from myadmin import views

urlpatterns = [
    # 后台首页
    url(r'^$', views.index, name="myadmin_index"),

    # 会员管理
    url(r'^user$', users.index, name="myadmin_user_index"),
    url(r'^users/add$', users.add, name="myadmin_users_add"),
    url(r'^users/insert$', users.insert, name="myadmin_users_insert"),
    url(r'^users/del/(?P<uid>[0-9]+)$', users.delete, name="myadmin_users_del"),
    url(r'^users/edit/(?P<uid>[0-9]+)$', users.edit, name="myadmin_users_edit"),
    url(r'^users/update/(?P<uid>[0-9]+)$', users.update, name="myadmin_users_update"),
    url(r'^dologin$', index.dologin, name="myadmin_dologin"),
    url(r'^logout$', index.logout, name="myadmin_logout"),
    url(r'^verify$', index.verify, name="myadmin_verify"),

    # 店铺管理
    url(r'^shop$', shops.index, name="myadmin_shop_index"),
    url(r'^shop/add$', shops.add, name="myadmin_shop_add"),
    url(r'^shop/insert$', shops.insert, name="myadmin_shop_insert"),
    url(r'^shop/del/(?P<uid>[0-9]+)$', shops.delete, name="myadmin_shop_del"),
    url(r'^shop/edit/(?P<uid>[0-9]+)$', shops.edit, name="myadmin_shop_edit"),
    url(r'^shop/update/(?P<uid>[0-9]+)$', shops.update, name="myadmin_shop_update"),

    #会员登陆
    url(r'^login$', index.login, name="myadmin_login"),

    #商品类别信息管理
    url(r'^category$', type.index, name="myadmin_type_index"),

    #商品信息管理
    url(r'^product$', goods.index, name="myadmin_goods_index"),


]