from django.shortcuts import render
from django.http import HttpResponse

from common.models import Shop
from datetime import datetime
import os,time


def index(request):
    # 执行数据查询，并放置到模板中
    list = Shop.objects.all()
    context = {"shopslist":list}
    print(context)
    #return HttpResponse(list)
    return render(request,'myadmin/shop/index.html',context)


def add(request):
    return render(request,'myadmin/shop/add.html')

 
def insert(request):
    '''执行添加'''
    try:
        # 执行图片上传处理
        myfile = request.FILES.get("cover_pic", None)
        if not myfile:
            return HttpResponse("没有上传文件信息")
        filename = str(time.time()) + "." + myfile.name.split('.').pop()
        destination = open("./static/myadmin/uploads/shop/"+filename,"wb+")
        for chunk in myfile.chunks():      # 分块写入文件  
            destination.write(chunk)          

        myfile_1 = request.FILES.get("banner_pic", None)
        if not myfile:
            return HttpResponse("没有上传文件信息")
        banner = str(time.time()) + "." + myfile.name.split('.').pop()
        destination = open("./static/myadmin/uploads/shop/"+filename,"wb+")
        for chunk in myfile.chunks():      # 分块写入文件  
            destination.write(chunk)  
        destination.close()

        # 保存商品信息
        ob = Shop()
        ob.name = request.POST['name']
        ob.phone = request.POST['phone']
        ob.address = request.POST['address']
        ob.cover_pic = filename
        ob.banner_pic = banner
        ob.state = 1
        ob.create_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        ob.update_at = None
        ob.save()
        context = {"info": "添加成功"}
    except Exception as err:
        print(err)
        context = {"info": "添加失败"}
    return render(request, "myadmin/info.html", context)

    
def delete(request, uid):
    try:
        # 获取被删除商品信的息量，先删除对应的图片
        ob = Shop.objects.get(id=uid)
        #执行图片删除
        os.remove("./static/myadmin/uploads/shop/" + ob.cover_pic)   
        #执行商品信息的删除 
        ob.delete()
        context = {'info':'删除成功！'}
    except Exception as err:
        print(err)
        context = {'info':'删除失败！'}
    return render(request,"myadmin/info.html",context)

def edit(request,uid):
    try:
        ob = Shop.objects.get(id=uid)
        #print(ob.name)
        #print("ok")
        context = {'shop':ob}
        return render(request,"myadmin/shop/edit.html",context)
    except Exception as err:
        print(err)
        context = {'info':'没有找到要修改的信息！'}
    return render(request,"myadmin/info.html",context)

def update(request,uid):
    try:
        ob = Shop.objects.get(id=uid)
        print(ob.name)
        ob.name = request.POST['name']
        ob.phone = request.POST['phone']
        ob.address = request.POST['address']
        ob.update_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        ob.save()
        context = {'info':'修改成功！'}
    except Exception as err:
        print(err)
        context = {'info':'修改失败！'}
    return render(request,"myadmin/info.html",context)