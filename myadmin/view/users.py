from django.shortcuts import render
from django.http import HttpResponse

from common.models import User
from datetime import datetime

# 浏览会员
def index(request):
    # 执行数据查询，并放置到模板中
    list = User.objects.all()
    context = {"userslist":list}
    #return HttpResponse(list)
    return render(request,'myadmin/user/index.html',context)

# 会员信息添加表单
def add(request):
    return render(request,'myadmin/user/add.html')

#执行会员信息添加    
def insert(request):
    try:
        ob = User()
        ob.username = request.POST['username']
        ob.nickname = request.POST['nickname']
        #获取密码并md5
        import hashlib
        m = hashlib.md5() 
        m.update(bytes(request.POST['password'],encoding="utf8"))
        ob.password_hash = m.hexdigest()
        ob.password_salt = request.POST['password']
        ob.status = 1
        ob.create_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        ob.update_at = None
        ob.save()
        context = {'info':'添加成功！'}
    except Exception as err:
        print(err)
        context = {'info':'添加失败！'}

    return render(request,"myadmin/info.html",context)

# 执行会员信息删除
def delete(request,uid):
    try:
        ob = User.objects.get(id=uid)
        ob.delete()
        context = {'info':'删除成功！'}
    except:
        context = {'info':'删除失败！'}
    return render(request,"myadmin/info.html",context)

# 打开会员信息编辑表单
def edit(request,uid):
    try:
        ob = User.objects.get(id=uid)
        #print("ok")
        context = {'user':ob}
        print(ob.username)
        print(ob.nickname)
        return render(request,"myadmin/user/edit.html",context)
    except Exception as err:
        print(err)
        context = {'info':'没有找到要修改的信息！'}
    return render(request,"myadmin/info.html",context)

# 执行会员信息编辑
def update(request,uid):
    try:
        ob = User.objects.get(id=uid)
        ob.username = request.POST['username']
        ob.nickname = request.POST['nickname']
        ob.update_at = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        ob.save()
        context = {'info':'修改成功！'}
    except Exception as err:
        print(err)
        context = {'info':'修改失败！'}
    return render(request,"myadmin/info.html",context)