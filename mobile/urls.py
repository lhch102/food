# myobject/mobile/urls.py
from django.conf.urls import url

from mobile.views import index

urlpatterns = [

    # 移动端首页（登录页）
    url(r'^$', index.login, name="mobile_login"),
    # 商户列表页
    url(r'^doLogin$', index.doLogin, name="mobile_doLogin"),
    # 菜品详情页
    url(r'^shopDetails/(?P<shopId>[0-9]+)/', index.detail, name="shop_details"),
    
    url(r'^order$', index.order, name="order"),

    url(r'^member$', index.member, name="member"),

]
