from django.shortcuts import render

from common.models import Shop, Category, Product
import simplejson
from django.http import HttpResponse


def login(request):
    """移动端首页"""
    return render(request, 'mobile/login.html')


def doLogin(request):
    """会员登录"""
    # 校验手机号
    mobile = request.POST['mobile']
    if mobile == '':
        context = {'info': '手机号不能为空！'}
        return render(request, "mobile/login.html", context)

    # 校验验证码
    verifycode = request.POST['code']
    if verifycode == '':
        context = {'info': '验证码不能为空！'}
        return render(request, "mobile/login.html", context)

    if verifycode != '1234':
        context = {'info': '验证码错误！'}
        return render(request, "mobile/login.html", context)

    # 此处登录成功，将当前登录手机号放入到session中
    request.session['member'] = mobile
    # 执行数据查询，并放置到模板中
    shopList = Shop.objects.all()
    context = {"shopList": shopList}
    return render(request, "mobile/shop/shop.html", context)


def detail(request, shopId):
    """商户详情"""

    context = {}
    shop = Shop.objects.get(id=shopId)
    shopName = shop.name
    request.session['shopName'] = shopName

    # 菜品分类
    category = Category.objects.filter(shop_id=shopId)

    p = []
    for c in category:

        # print("id:"+str(c.id))
        # print("shopId:"+str(shopId))
        # 菜品
        p += Product.objects.filter(shop_id=shopId, category_id=c.id)

    context['product'] = p
    context['category'] = category
    context['shopName'] = shopName
    return render(request, "mobile/shop/shopDetail.html", context)

def order(request):
    context = {}
    cartlist = request.POST['cartlist']
    req=simplejson.loads(cartlist)
    context['cartlist'] = req
    return render(request, "mobile/order.html", context)


def member(request):
    '''个人中心（我的）'''
    return render(request, "mobile/member.html")
